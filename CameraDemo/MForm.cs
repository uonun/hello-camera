﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;

namespace CameraDemo
{
    public partial class MForm : Form
    {
        private Dictionary<string, short> _cameras = new Dictionary<string, short>();
        private IntPtr _handle;
        private short _idx;
        private CameraApi.FrameEventHandler mFrameEventHandler; // Delegate instance for the frame callback - must keep alive! gc should NOT collect it

        public MForm()
        {
            InitializeComponent();
        }

        private void buSearchCamera_Click(object sender, EventArgs e)
        {
            cbCameras.Items.Clear();
            _cameras.Clear();

            byte[] lpszName = new byte[100];
            byte[] lpszVer = new byte[100];

            bool x = false;
            short n = 0;
            do
            {
                x = CameraApi.capGetDriverDescription(n, lpszName, lpszName.Length, lpszVer, lpszVer.Length);
                if (x)
                {
                    string name = Encoding.Default.GetString(lpszName).Trim('\0');
                    string ver = Encoding.Default.GetString(lpszVer).Trim('\0');

                    string camName = string.Format("{0}({1})", name, ver);
                    cbCameras.Items.Add(camName);
                    _cameras.Add(camName, n);

                    n++;
                }
            } while (x && n < 10);

            if (cbCameras.Items.Count > 0) cbCameras.SelectedIndex = 0;
        }

        private void buOpenCamera_Click(object sender, EventArgs e)
        {
            if (_cameras.Count < 1)
            {
                MessageBox.Show("请先选择摄像头！");
                return;
            }

            int left = 1;
            int top = 1;
            int width = videoWindow.Width - 2;
            int height = videoWindow.Height - 2;

            _handle = CameraApi.capCreateCaptureWindow("Camera Window", CameraApi.WS_CHILD | CameraApi.WS_VISIBLE, left, top, width, height, videoWindow.Handle, 0);
            if (null != _handle)
            {
                short index = _cameras[cbCameras.Text];
                _idx = index;

                bool x = false;
                int tmp = 0;
                do
                {
                    x = CameraApi.capDriverConnect(_handle, index);
                    tmp++;
                }
                while (!x && tmp < 100);
                Console.WriteLine("tmp = " + tmp.ToString());
                if (x)
                {
                    x = CameraApi.capPreviewRate(_handle, 66);
                    x = CameraApi.capPreview(_handle, true);
                    //CameraApi.BITMAPINFO bitmapinfo = new CameraApi.BITMAPINFO();
                    //bitmapinfo.bmiHeader.biSize = Marshal.SizeOf(bitmapinfo.bmiHeader);
                    //bitmapinfo.bmiHeader.biWidth = width;
                    //bitmapinfo.bmiHeader.biHeight = height;
                    //bitmapinfo.bmiHeader.biPlanes = 1;
                    //bitmapinfo.bmiHeader.biBitCount = 24;
                    //CameraApi.capSetVideoFormat(_handle, ref bitmapinfo, Marshal.SizeOf(bitmapinfo));
                    //this.mFrameEventHandler = new CameraApi.FrameEventHandler(FrameCallBack);
                    //CameraApi.capSetCallbackOnFrame(_handle, this.mFrameEventHandler);
                    //CameraApi.SetWindowPos(_handle, 0, 0, 0, width, height, 6);
                }

                /*
                //CameraApi.SendMessage(_handle, CameraApi.WM_CAP_SET_CALLBACK_VIDEOSTREAM, 0, 0);
                //CameraApi.SendMessage(_handle, CameraApi.WM_CAP_SET_CALLBACK_ERROR, 0, 0);
                //CameraApi.SendMessage(_handle, CameraApi.WM_CAP_SET_CALLBACK_STATUSA, 0, 0);
                CameraApi.SendMessage(_handle, CameraApi.WM_CAP_DRIVER_CONNECT, _idx, 0);   // necessary
                //CameraApi.SendMessage(_handle, CameraApi.WM_CAP_SET_SCALE, 1, 0);
                CameraApi.SendMessage(_handle, CameraApi.WM_CAP_SET_PREVIEWRATE, 66, 0);    // necessary
                //CameraApi.SendMessage(_handle, CameraApi.WM_CAP_SET_OVERLAY, 1, 0);
                CameraApi.SendMessage(_handle, CameraApi.WM_CAP_SET_PREVIEW, 1, 0);         // necessary
                 * */
            }
            else
            {
                MessageBox.Show("打开失败！");
            }
        }

        private void buCloseCamera_Click(object sender, EventArgs e)
        {
            if (null == _handle) return;
            CameraApi.SendMessage(_handle, CameraApi.WM_CAP_DRIVER_DISCONNECT, 66, 0);
        }

        private void FrameCallBack(IntPtr lwnd, IntPtr lpVHdr)
        {
            //CameraApi.VIDEOHDR videoHeader = new CameraApi.VIDEOHDR();
            //byte[] VideoData;
            //videoHeader = (CameraApi.VIDEOHDR)CameraApi.GetStructure(lpVHdr, videoHeader);
            //VideoData = new byte[videoHeader.dwBytesUsed];
            //CameraApi.Copy(videoHeader.lpData, VideoData);
            //if (this.RecievedFrame != null)
            //    this.RecievedFrame(VideoData);
        }

        protected override void OnResize(EventArgs e)
        {
            int width = videoWindow.Width - 2;
            int height = videoWindow.Height - 2;
            CameraApi.SetWindowPos(_handle, 0, 0, 0, width, height, 6);
            base.OnResize(e);
        }
    }
}
