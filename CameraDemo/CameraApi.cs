﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace CameraDemo
{
    class CameraApi
    {

        public const int WM_USER = 0x400;
        public const int WS_CHILD = 0x40000000;
        public const int WS_VISIBLE = 0x10000000;
        public const int WM_CAP_START = WM_USER;
        public const int WM_CAP_STOP = WM_CAP_START + 68;
        public const int WM_CAP_DRIVER_CONNECT = WM_CAP_START + 10;
        public const int WM_CAP_DRIVER_DISCONNECT = WM_CAP_START + 11;
        public const int WM_CAP_SAVEDIB = WM_CAP_START + 25;
        public const int WM_CAP_GRAB_FRAME = WM_CAP_START + 60;
        public const int WM_CAP_SEQUENCE = WM_CAP_START + 62;
        public const int WM_CAP_FILE_SET_CAPTURE_FILEA = WM_CAP_START + 20;
        public const int WM_CAP_SEQUENCE_NOFILE = WM_CAP_START + 63;
        public const int WM_CAP_SET_OVERLAY = WM_CAP_START + 51;
        public const int WM_CAP_SET_PREVIEW = WM_CAP_START + 50;
        public const int WM_CAP_SET_CALLBACK_VIDEOSTREAM = WM_CAP_START + 6;
        public const int WM_CAP_SET_CALLBACK_ERROR = WM_CAP_START + 2;
        public const int WM_CAP_SET_CALLBACK_STATUSA = WM_CAP_START + 3;
        public const int WM_CAP_SET_CALLBACK_FRAME = WM_CAP_START + 5;
        public const int WM_CAP_SET_SCALE = WM_CAP_START + 53;
        public const int WM_CAP_SET_PREVIEWRATE = WM_CAP_START + 52;
        public const int WM_CAP_SET_VIDEOFORMAT = WM_USER + 45;

        [DllImport("avicap32.dll")]
        public static extern bool capGetDriverDescription(short wDriver, byte[] lpszName, int cbName, byte[] lpszVer, int cbVer);

        [DllImport("avicap32.dll")]
        public static extern IntPtr capCreateCaptureWindow(string lpszWindowName, int dwStyle, int x, int y, int nWidth, int nHeight, IntPtr hWndParent, int nID);

        [DllImport("User32.dll")]
        public static extern bool SendMessage(IntPtr hWnd, int wMsg, bool wParam, int lParam);
        [DllImport("User32.dll")]
        public static extern bool SendMessage(IntPtr hWnd, int wMsg, short wParam, int lParam);
        [DllImport("User32.dll")]
        public static extern bool SendMessage(IntPtr hWnd, int wMsg, short wParam, FrameEventHandler lParam);
        [DllImport("User32.dll")]
        public static extern bool SendMessage(IntPtr hWnd, int wMsg, int wParam, ref BITMAPINFO lParam);

        [DllImport("User32.dll")]
        public static extern int SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int y, int cx, int cy, int wFlags);

        public static bool capDriverConnect(IntPtr lwnd, short i)
        {
            return SendMessage(lwnd, WM_CAP_DRIVER_CONNECT, i, 0);
        }

        public static bool capDriverDisconnect(IntPtr lwnd)
        {
            return SendMessage(lwnd, WM_CAP_DRIVER_DISCONNECT, 0, 0);
        }

        public static bool capPreview(IntPtr lwnd, bool f)
        {
            return SendMessage(lwnd, WM_CAP_SET_PREVIEW, f, 0);
        }

        public static bool capPreviewRate(IntPtr lwnd, short wMS)
        {
            return SendMessage(lwnd, WM_CAP_SET_PREVIEWRATE, wMS, 0);
        }

        public static bool capSetVideoFormat(IntPtr hCapWnd, ref BITMAPINFO BmpFormat, int CapFormatSize)
        {
            return SendMessage(hCapWnd, WM_CAP_SET_VIDEOFORMAT, CapFormatSize, ref BmpFormat);
        }

        public static bool capSetCallbackOnFrame(IntPtr lwnd, FrameEventHandler lpProc)
        {
            return SendMessage(lwnd, WM_CAP_SET_CALLBACK_FRAME, 0, lpProc);
        }

        public delegate void FrameEventHandler(IntPtr lwnd, IntPtr lpVHdr);

        [StructLayout(LayoutKind.Sequential)]
        public struct BITMAPINFOHEADER
        {
            [MarshalAs(UnmanagedType.I4)]
            public Int32 biSize;
            [MarshalAs(UnmanagedType.I4)]
            public Int32 biWidth;
            [MarshalAs(UnmanagedType.I4)]
            public Int32 biHeight;
            [MarshalAs(UnmanagedType.I2)]
            public short biPlanes;
            [MarshalAs(UnmanagedType.I2)]
            public short biBitCount;
            [MarshalAs(UnmanagedType.I4)]
            public Int32 biCompression;
            [MarshalAs(UnmanagedType.I4)]
            public Int32 biSizeImage;
            [MarshalAs(UnmanagedType.I4)]
            public Int32 biXPelsPerMeter;
            [MarshalAs(UnmanagedType.I4)]
            public Int32 biYPelsPerMeter;
            [MarshalAs(UnmanagedType.I4)]
            public Int32 biClrUsed;
            [MarshalAs(UnmanagedType.I4)]
            public Int32 biClrImportant;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct BITMAPINFO
        {
            [MarshalAs(UnmanagedType.Struct, SizeConst = 40)]
            public BITMAPINFOHEADER bmiHeader;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1024)]
            public Int32[] bmiColors;
        }
    }
}
