﻿namespace CameraDemo
{
    partial class MForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.videoWindow = new System.Windows.Forms.Panel();
            this.buSearchCamera = new System.Windows.Forms.Button();
            this.cbCameras = new System.Windows.Forms.ComboBox();
            this.buOpenCamera = new System.Windows.Forms.Button();
            this.buCloseCamera = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // videoWindow
            // 
            this.videoWindow.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.videoWindow.BackColor = System.Drawing.Color.Black;
            this.videoWindow.Location = new System.Drawing.Point(12, 41);
            this.videoWindow.Name = "videoWindow";
            this.videoWindow.Size = new System.Drawing.Size(440, 335);
            this.videoWindow.TabIndex = 0;
            // 
            // buSearchCamera
            // 
            this.buSearchCamera.Location = new System.Drawing.Point(12, 12);
            this.buSearchCamera.Name = "buSearchCamera";
            this.buSearchCamera.Size = new System.Drawing.Size(75, 23);
            this.buSearchCamera.TabIndex = 1;
            this.buSearchCamera.Text = "搜索摄像头";
            this.buSearchCamera.UseVisualStyleBackColor = true;
            this.buSearchCamera.Click += new System.EventHandler(this.buSearchCamera_Click);
            // 
            // cbCameras
            // 
            this.cbCameras.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCameras.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCameras.FormattingEnabled = true;
            this.cbCameras.Location = new System.Drawing.Point(93, 14);
            this.cbCameras.Name = "cbCameras";
            this.cbCameras.Size = new System.Drawing.Size(521, 20);
            this.cbCameras.TabIndex = 2;
            // 
            // buOpenCamera
            // 
            this.buOpenCamera.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buOpenCamera.Location = new System.Drawing.Point(458, 41);
            this.buOpenCamera.Name = "buOpenCamera";
            this.buOpenCamera.Size = new System.Drawing.Size(75, 23);
            this.buOpenCamera.TabIndex = 3;
            this.buOpenCamera.Text = "打开摄像头";
            this.buOpenCamera.UseVisualStyleBackColor = true;
            this.buOpenCamera.Click += new System.EventHandler(this.buOpenCamera_Click);
            // 
            // buCloseCamera
            // 
            this.buCloseCamera.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buCloseCamera.Location = new System.Drawing.Point(539, 41);
            this.buCloseCamera.Name = "buCloseCamera";
            this.buCloseCamera.Size = new System.Drawing.Size(75, 23);
            this.buCloseCamera.TabIndex = 4;
            this.buCloseCamera.Text = "关闭摄像头";
            this.buCloseCamera.UseVisualStyleBackColor = true;
            this.buCloseCamera.Click += new System.EventHandler(this.buCloseCamera_Click);
            // 
            // MForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(626, 388);
            this.Controls.Add(this.buCloseCamera);
            this.Controls.Add(this.buOpenCamera);
            this.Controls.Add(this.cbCameras);
            this.Controls.Add(this.buSearchCamera);
            this.Controls.Add(this.videoWindow);
            this.Name = "MForm";
            this.Text = "摄像头操作测试";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel videoWindow;
        private System.Windows.Forms.Button buSearchCamera;
        private System.Windows.Forms.ComboBox cbCameras;
        private System.Windows.Forms.Button buOpenCamera;
        private System.Windows.Forms.Button buCloseCamera;
    }
}

